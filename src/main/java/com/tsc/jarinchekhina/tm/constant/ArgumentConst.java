package com.tsc.jarinchekhina.tm.constant;

public interface ArgumentConst {

    String ABOUT = "-a";

    String HELP = "-h";

    String VERSION = "-v";

    String INFO = "-i";

    String COMMANDS = "-cmd";

    String ARGUMENTS = "-arg";

}

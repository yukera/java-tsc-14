package com.tsc.jarinchekhina.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void startTaskById();

    void startTaskByIndex();

    void startTaskByName();

    void finishTaskById();

    void finishTaskByIndex();

    void finishTaskByName();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void changeTaskStatusByName();

    void findAllTaskByProjectId();

    void bindTaskByProjectId();

    void unbindTaskByProjectId();

}

package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    void clearProjects();

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskByProjectId(String taskId);

    Project removeProjectById(String projectId);

}
